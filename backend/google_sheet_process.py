#!/usr/bin/env python
# -*- coding: utf-8 -*-
import config.google_sheet_config as config
import csv
import logging
import logging.handlers
import os
import sys
import time
import traceback
import importlib

from authentication import Authentication
from datetime import datetime, date
from abstract_process import AbstractProcess
from sanitizer import Sanitizer


class GoogleSheetProcess(AbstractProcess):
    def __init__(self):
        super(GoogleSheetProcess, self).__init__("GOOGLE_SHEET")
        self.__setup_logger()

    def __setup_logger(self):
        """
        Inicializador de log para todo o processo de aquisição e upload de dados
        """
        log_path = "logs"
        if not os.path.exists(log_path):
            os.makedirs(log_path)

        log_handler = logging.handlers.WatchedFileHandler(
            "logs/{0}_log_{1}.log".format(
                "GOOGLE_SHEET",
                date.today().strftime('%d%m%Y')
            )
        )
        formatter = logging.Formatter(
            '[%(levelname)s] %(asctime)s - %(name)s \n\t%(message)s',
            '%Y/%m/%d %H:%M:%S'
        )

        formatter.converter = time.gmtime
        log_handler.setFormatter(formatter)
        logger = logging.getLogger()
        logger.handlers = []
        logger.addHandler(log_handler)
        logger.setLevel(logging.DEBUG)

    def __setup(self):
        """
        Inicializa ambiente para executar o processo
        :type local_folder: str
        """
        self.__setup_logger()

        logging.debug("[GOOGLE_SHEET] Reconfiguração de ambiente iniciada")
        stdin, stdout = sys.stdin, sys.stdout
        importlib.reload(sys)
        sys.stdin, sys.stdout = stdin, stdout
        logging.debug("[GOOGLE_SHEET] Reconfiguração de ambiente finalizada")

    def __setup_folder(self, local_folder):
        """
        Cria diretorio para armazenar os CSV de acordo com o cliente
        :type local_folder: str
        """
        if not os.path.exists(local_folder):
            os.makedirs(local_folder)

    def google_sheet_process(
            self,
            report_config
    ):
        """
        Inicia e coordena todo o processo que envolve aquisição de dados
        :type report_config: dict
        """
        self.__setup()

        service = Authentication().get_service_from_source("SHEET")

        for reports in filter(lambda r: r.enabled, report_config):
            spreadsheet_range = "{0}!{1}:{2}".format(
                reports.sheet_name,
                reports.first_column_index,
                reports.last_column_index
            )

            logging.debug("Retornando dados do Google Spreadsheet. ID: {0}".format(reports.spreadsheet_id))

            spreadsheet_result = service.spreadsheets().values().get(
                spreadsheetId=reports.spreadsheet_id,
                range=spreadsheet_range
            ).execute()

            sheet_values = spreadsheet_result.get('values', [])

            if not sheet_values:
                print("Dados não localizados. Sheet Range: {0}".format(spreadsheet_range))
                logging.debug("Dados não localizados. Sheet Range: {0}".format(spreadsheet_range))
            else:
                template_filename = reports.table_file
                local_folder = reports.local_folder

                logging.debug("Iniciando Report: {0}".format(template_filename))

                logging.debug("Formatando o nome do arquivo local")

                file_name = "{0}_{1}.csv".format(
                    template_filename,
                    datetime.now().strftime("%Y%m%d%H%M%S%f")
                )

                logging.debug("Formatação concluida: {0}".format(file_name))

                logging.debug("Formatando caminho completo local")

                csv_path = "{0}\\{1}".format(
                    os.getcwd(),
                    local_folder
                )

                self.__setup_folder(csv_path)

                csv_path = "{0}\\{1}".format(
                    csv_path,
                    file_name
                )

                logging.debug("Formatação concluida: {0}".format(csv_path))

                logging.debug("Abrindo para escrita o arquivo CSV local")

                response_media = sheet_values

                line_width = len(response_media[0])
                # Adaptacao dos registros para funcionar no CSV e SQL
                """
                for i in range(len(response_media)):
                    response_media[i] = [
                        Sanitizer.normalize_unicode_data(r.replace('|', '/'), "sql", limit_char=500)
                        for r in response_media[i]
                    ]
                    # response_media[i] = Sanitizer.normalize_unicode_data(response_media[i], "sql", limit_char=500)
                    if len(response_media[i]) < line_width:
                        blank_list = [''] * (line_width - len(response_media[i]))
                        response_media[i].extend(blank_list)
                    # response_media[i] = ';'.join(response_media[i])
                    # response_media[i] = Sanitizer.clean_semicolon_between_quotes_only(str(response_media[i]))
                """
                with open(csv_path, 'w', newline='', encoding='utf-8') as csv_file:
                    csv_writer = csv.writer(
                        csv_file,
                        delimiter=";",
                        dialect="excel"
                    )
                    for rows in response_media[:]:
                        # csv_writer.writerow(rows.split(";"))
                        csv_writer.writerow(rows)

                    logging.debug("Processo de escrita finalizado")

                csv_file.close()
                logging.debug("Escrita do arquivo CSV local finalizada, arquivo fechado")

    def acquisition(self):
        """
        Executa o processo de criação de arquivo CSV a partir de uma fonte de dados
        """
        process = GoogleSheetProcess()
        for report in config.projects["GOOGLE_SHEET"]:
            report_config = report["cfg"]
            try:
                process.google_sheet_process(
                    report_config
                )
            except Exception as ex:
                logging.exception(ex)

    def mssql_store(self, project=config.projects["GOOGLE_SHEET"], bulk_insert=True, field_terminator="|"):
        super(GoogleSheetProcess, self).mssql_store(project=project, field_terminator=field_terminator)


if __name__ == "__main__":
    process = GoogleSheetProcess()
    for report in config.projects["GOOGLE_SHEET"]:
        report_config = report["cfg"]
        try:
            process.google_sheet_process(
                report_config
            )
        except Exception as ex:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
            msg = ''.join('!! ' + line for line in lines)
            logging.exception(msg)
            print(msg)
