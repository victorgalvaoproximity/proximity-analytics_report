#!/usr/bin/env python
# -*- coding: utf-8 -*-
import httplib2
import os

from googleapiclient.discovery import build
from googleads import adwords
from googleads import oauth2
from oauth2client.client import OAuth2WebServerFlow
from oauth2client.service_account import ServiceAccountCredentials
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client import tools


class Authentication:
    LOCAL_PATH = os.path.dirname(os.path.abspath(__file__))

    def __init__(self):
        pass

    # Google services authentication methods
    def __get_http_authorize(
            self,
            credentials
    ):
        """
        Realiza a autenticacao web para os certificados de servicos Google
        :type credentials: OAuth2Credentials
        """
        http = credentials.authorize(httplib2.Http())

        return http

    def __get_credentials_signed_assertion(
            self,
            p12_file_path,
            service_account_email,
            scope
    ):
        """
        Gera o credentials de acordo com o arquivo .p12 da service account (Google Analytics e Drive)
        :type p12_file_path: str
        :type service_account_email: str
        :type scope: str
        """
        with open(p12_file_path, "rb") as key_file:
            key = key_file.read()
        key_file.close()
        credentials = ServiceAccountCredentials.from_p12_keyfile(service_account_email, p12_file_path, scopes=scope)
        return credentials

    def __get_credentials_web_server_flow(
            self,
            storage_path,
            client_id,
            client_secret,
            scope,
            redirect_uri
    ):
        """
        Gera o credentials de acordo com o client id e o client secret, ou utiliza o credentials armazenado se ainda
        valido (Google DBM e DCM)
        :type storage_path: str
        :type client_id: str
        :type client_secret: str
        :type scope: str
        :type redirect_uri: str
        :type enable_debug_web_proxy: bool
        """
        storage = Storage(storage_path)
        credentials = storage.get()
        if credentials is None or credentials.invalid is True or credentials.access_token_expired is True:
            if credentials is None or credentials.refresh_token is None:
                flow = OAuth2WebServerFlow(
                    client_id=client_id,
                    client_secret=client_secret,
                    scope=scope,
                    redirect_uri=redirect_uri
                )
                credentials = tools.run_flow(flow, storage, tools.argparser.parse_args([]))
            else:
                http = self.__get_http_authorize(credentials)
                credentials.refresh(http=http)
        return credentials

    def __get_credentials_client_secret_flow(
            self,
            storage_path,
            client_secret_path,
            scope
    ):
        """
        Gera o credentials de acordo com o arquivo client_secret.json, ou utiliza o credentials armazenado se ainda
        valido (Google Storage e Big Query)
        :type storage_path: str
        :type client_secret_path: str
        :type scope: str
        """
        storage = Storage(storage_path)
        credentials = storage.get()
        if credentials is None or credentials.invalid:
            flow = flow_from_clientsecrets(
                client_secret_path,
                scope=scope
            )
            credentials = tools.run_flow(flow, storage, tools.argparser.parse_args([]))
        return credentials

    def get_service_from_source(
            self,
            source_name
    ):
        """
        Retorna o objeto para utilizacao dos servicos das APIs Google, ja autenticado
        :type source_name: str
        :type enable_debug_web_proxy: bool
        """
        credentials = None
        service = None
        if all_sources[source_name]:
            source_properties = all_sources[source_name]
            if source_properties["function"] == "ServiceAccountCredentials":  # GA & Drive
                key_file_path = "{0}{1}".format(self.LOCAL_PATH, source_properties["key_file_path"])
                credentials = self.__get_credentials_signed_assertion(
                    key_file_path,
                    source_properties["service_account_email"],
                    source_properties["scope"]
                )
            elif source_properties["function"] == "OAuth2WebServerFlow":  # DBM & DCM
                storage_path = "{0}{1}".format(self.LOCAL_PATH, source_properties["storage_path"])
                credentials = self.__get_credentials_web_server_flow(
                    storage_path,
                    source_properties["client_id"],
                    source_properties["client_secret"],
                    source_properties["scope"],
                    source_properties["redirect_uri"]
                )
            elif source_properties["function"] == "flow_from_clientsecrets":  # Sheet
                storage_path = "{0}{1}".format(self.LOCAL_PATH, source_properties["storage_path"])
                secret_path = "{0}{1}".format(self.LOCAL_PATH, source_properties["secret_path"])
                credentials = self.__get_credentials_client_secret_flow(
                    storage_path,
                    secret_path,
                    source_properties["scope"]
                )
            if credentials:
                http = self.__get_http_authorize(credentials)
                service = build(
                    source_properties["build_name"],
                    source_properties["build_version"],
                    http=http
                )
        return service

    def get_adwords_client(self, client_customer_id):
        source_properties = all_sources["GDN"]
        oauth2_client = oauth2.GoogleRefreshTokenClient(
            client_id=source_properties["client_id"],
            client_secret=source_properties["client_secret"],
            refresh_token=source_properties["refresh_token"]
        )
        adwords_client = adwords.AdWordsClient(
            developer_token=source_properties["developer_token"],
            oauth2_client=oauth2_client,
            user_agent=source_properties["user_agent"],
            client_customer_id=client_customer_id
        )
        return adwords_client


# Base para autenticacao de GA V3 e V4, o scope e dados de auth sao os mesmos, as duas API sao usadas em conjunto
google_analytics = dict(
    function="ServiceAccountCredentials",
    key_file_path="\\secret\\bi-reports-500862d44adc.p12",
    service_account_email="datagrabber@bi-reports-228020.iam.gserviceaccount.com",
    scope=['https://www.googleapis.com/auth/analytics', 'https://www.googleapis.com/auth/analytics.edit']
)

google_analytics = google_analytics.copy()
google_analytics.update(
    build_name="analytics",
    build_version="v3"
)

google_analytics_v4 = google_analytics.copy()
google_analytics_v4.update(
    build_name="analyticsreporting",
    build_version="v4"
)

google_drive = dict(
    function="ServiceAccountCredentials",
    key_file_path="\\secret\\bi-reports-500862d44adc.p12",
    service_account_email="datagrabber@bi-reports-228020.iam.gserviceaccount.com",
    scope=['https://www.googleapis.com/auth/drive'],
    build_name="drive",
    build_version="v2"
)

google_dbm = dict(
    function="OAuth2WebServerFlow",
    client_id="",
    client_secret="",
    redirect_uri="",
    storage_path="\\secret\\CREDENTIALS.dat",
    scope=['https://www.googleapis.com/auth/doubleclickbidmanager'],
    build_name="doubleclickbidmanager",
    build_version="v1"
)

google_dcm = dict(
    function="OAuth2WebServerFlow",
    client_id="",
    client_secret="",
    redirect_uri="",
    storage_path="\\secret\\CREDENTIALS.dat",
    scope=[
        'https://www.googleapis.com/auth/dfareporting',
        'https://www.googleapis.com/auth/dfatrafficking',
        'https://www.googleapis.com/auth/doubleclickbidmanager'
    ],
    build_name="dfareporting",
    build_version="v3.2"
)

google_gdn_adwords = dict(
    developer_token="UpzxmfKinWggpGZZ8T91pA",
    user_agent="userAgent",
    client_id="568290907498-2135uq3due3i2od4kcmnf4ctin2pv04t.apps.googleusercontent.com",
    client_secret="yO5_7B-xQ7p4DVqZmFCdaM3h",
    refresh_token="1/hXQ_I2L-metB9bGZBah-tjH2OLVfCNtsahM0gQ5ul60"
)

google_sheet = dict(
    function="flow_from_clientsecrets",
    secret_path="\\secret\\client_secret_940594994243-4pf1fqfnpaghfp6eef6tc4stbkj26e0c.apps.googleusercontent.com.json",
    storage_path="\\secret\\credentials_sheet.dat",
    scope=["https://www.googleapis.com/auth/spreadsheets.readonly"],
    build_name="sheets",
    build_version="v4"
)

sql_server = dict(
    host="SERVER_ADDRESS",
    user="USER",
    password="PASS"
)

all_sources = dict(
    GA=google_analytics,
    GAV4=google_analytics_v4,
    DBM=google_dbm,
    DCM=google_dcm,
    DRIVE=google_drive,
    GDN=google_gdn_adwords,
    SHEET=google_sheet,
    SQLSERVER=sql_server
)
