#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import config.main_config as config
import sys

from gav4_process import GaV4Process
from gdn_process import GdnProcess
from google_sheet_process import GoogleSheetProcess

parser = argparse.ArgumentParser()

parser.add_argument('--process', type=str, help='Nome do processo a executar: GAV4|GDN|SHEET')
parser.add_argument('--step', type=str, help='Nome da etapa a executar: ALL|ACQUISITION|SQL_STORE')
parser.add_argument('--debug', type=bool, help='Habilita aplicacao em modo debug: True|False', default=False)

args = parser.parse_args()

if args.process and args.step:
    config.enable_web_proxy = bool(args.debug)

    if args.process == "GAV4":
        process = GaV4Process()
    elif args.process == "GDN":
        process = GdnProcess()
    elif args.process == "SHEET":
        process = GoogleSheetProcess()
    else:
        sys.exit(0)

    if args.step == "ALL":
        process.acquisition()
        process.mssql_store()

    elif args.step == "ACQUISITION":
        process.acquisition()

    elif args.step == "SQL_STORE":
        process.mssql_store()
