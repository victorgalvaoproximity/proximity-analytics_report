# coding=utf-8
import re
import unicodedata
import urllib
import urllib.parse as parse


class Sanitizer:
    def __init__(self):
        pass

    @staticmethod
    def remove_non_ascii(dirty_string):
        """
        Remove da string caracteres não pertencentes a tabela ascii
        :rtype : str
        :type dirty_string: str
        """
        return "".join(i for i in dirty_string if ord(i) < 128)

    @staticmethod
    def normalize_unicode_data(data, db_type="", limit_char=0):
        """
        Executa processo de normatização para dados unicode
        :type data: str | unicode
        :type db_type: str | unicode
        :type limit_char: int
        """
        if db_type == "sql":  # normalizando para carga em SQL Server
            data = Sanitizer.clean_semicolon_between_quotes(data)
            data = parse.unquote(parse.unquote(data)).encode('utf-8', 'replace')
            data = data.decode("utf-8")  # .replace(';', ",")
            if limit_char > 0:
                data = data[:limit_char]
            return data
        else:
            return unicodedata.normalize('NFKD', data).encode('ascii', 'ignore')

    @staticmethod
    def clean_semicolon_between_quotes(dirty_string):
        """
        Limpeza para carga de CSV separados por ponto e virgula
        :type dirty_string: string
        """
        clean_string = dirty_string
        re_value = ',' + '(?!(([^"]*"){2})*[^"]*$)'
        while re.search(re_value, clean_string) is not None:
            clean_string = re.sub(re_value, r"{0}".format('||'), clean_string)
        clean_string = clean_string.replace(",", ";")
        clean_string = clean_string.replace("||", ",")
        clean_string = clean_string.replace('"', "")
        return clean_string

    @staticmethod
    def clean_char_between_quotes(
            dirty_string,
            char_to_clean=",",
            char_to_replace=";"
    ):
        """
        Limpeza para carga de CSV de colunas com caracteres especificos em valores entre aspas
        :type dirty_string: string
        :type char_to_clean: string
        :type char_to_replace: string
        """
        clean_string = dirty_string
        re_value = char_to_clean + '(?!(([^"]*"){2})*[^"]*$)'
        while re.search(re_value, clean_string) is not None:
            clean_string = re.sub(re_value, r"{0}".format(char_to_replace), clean_string)
        clean_string = clean_string.replace('"', "")
        return clean_string

    @staticmethod
    def sanitize_string(dirty_string):
        """
        Converte caracteres acentuados para não acentuados, '{}[]()-' para "vazio" e '\/vazio'  para "underscore"
        :rtype : str | unicode
        :type dirty_string: str | unicode
        """
        # if isinstance(dirty_string, unicode):
        # return Sanitizer.normalize_unicode_data(Sanitizer.sanitize_case_string(dirty_string))
        clean_string = dirty_string.lower()
        """
        clean_string = dirty_string.lower() \
            .replace("{", "").replace("}", "") \
            .replace("(", "").replace(")", "") \
            .replace("[", "").replace("]", "") \
            .replace(":", "").replace("=", "") \
            .replace("\\", "_").replace("/", "_") \
            .replace(" ", "_").replace("__", "_") \
            .replace("-", "")
        clean_string = Sanitizer.sanitize_case_string(clean_string)
        """
        return Sanitizer.remove_non_ascii(clean_string)

    @staticmethod
    def sanitize_case_string(dirty_string):
        """
        Converte caracteres acentuados para não acentuados
        :rtype : str | unicode
        :type dirty_string: str | unicode
        """
        clean_string = dirty_string.replace("ç", "c").replace("Ç", "C") \
            .replace("á", "a").replace("à", "a") \
            .replace("ã", "a").replace("ä", "a") \
            .replace("â", "a").replace("ê", "e") \
            .replace("é", "e").replace("è", "e") \
            .replace("ë", "e").replace("í", "i") \
            .replace("ì", "i").replace("ï", "i") \
            .replace("ó", "o").replace("ò", "o") \
            .replace("ô", "o").replace("õ", "o") \
            .replace("ö", "o").replace("ú", "u") \
            .replace("ù", "u").replace("ü", "u") \
            .replace("Á", "A").replace("À", "A") \
            .replace("Ã", "A").replace("Ä", "A") \
            .replace("Â", "A").replace("Ê", "E") \
            .replace("É", "E").replace("È", "E") \
            .replace("Ë", "E").replace("Í", "I") \
            .replace("Ì", "I").replace("Ï", "I") \
            .replace("Ó", "O").replace("Ò", "O") \
            .replace("Ô", "O").replace("Õ", "O") \
            .replace("Ö", "O").replace("Ú", "U") \
            .replace("Ù", "U").replace("Ü", "U")
        return clean_string

    @staticmethod
    def normalize_response_media(
            response_media,
            need_encode=False,
            need_fix_columns=False,
            need_sql_adjustment=False,
            split_line="\n"
    ):
        """
        Remove caracteres que podem quebrar a formatação do CSV
        :rtype : list
        :type response_media: str | unicode
        :type need_encode: bool
        :type need_fix_columns: bool
        :type need_sql_adjustment: bool
        :type split_line: str
        """
        if need_encode:
            response_media = response_media.encode('windows-1252')

        response_media = response_media\
            .strip()\
            .split(split_line)

        if need_sql_adjustment:
            for i in range(len(response_media)):
                response_media[i] = Sanitizer.normalize_unicode_data(str(response_media[i]), "sql")

        if need_fix_columns:
            for i in range(len(response_media)):
                response_media[i] = Sanitizer.clean_char_between_quotes(str(response_media[i]))

        return response_media
