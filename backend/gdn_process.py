#!/usr/bin/env python
# -*- coding: utf-8 -*-
import config.gdn_config as config
import csv
import logging
import logging.handlers
import os
import re
import sys
import time
import traceback
import importlib

from authentication import Authentication
from datetime import datetime, date
from abstract_process import AbstractProcess
from sanitizer import Sanitizer


class GdnProcess(AbstractProcess):
    def __init__(self):
        super(GdnProcess, self).__init__("GDN")
        self.__setup_logger()

    def __setup_logger(self):
        """
        Inicializador de log para todo o processo de aquisição e upload de dados
        """
        log_path = "logs"
        if not os.path.exists(log_path):
            os.makedirs(log_path)

        log_handler = logging.handlers.WatchedFileHandler(
            "logs/{0}_log_{1}.log".format(
                "GDN",
                date.today().strftime('%d%m%Y')
            )
        )
        formatter = logging.Formatter(
            '[%(levelname)s] %(asctime)s - %(name)s \n\t%(message)s',
            '%Y/%m/%d %H:%M:%S'
        )

        formatter.converter = time.gmtime
        log_handler.setFormatter(formatter)
        logger = logging.getLogger()
        logger.handlers = []
        logger.addHandler(log_handler)
        logger.setLevel(logging.DEBUG)

    def __setup(self):
        """
        Inicializa ambiente para executar o processo
        :type local_folder: str
        """
        self.__setup_logger()

        logging.debug("[GDN] Reconfiguração de ambiente iniciada")
        stdin, stdout = sys.stdin, sys.stdout
        importlib.reload(sys)
        sys.stdin, sys.stdout = stdin, stdout
        logging.debug("[GDN] Reconfiguração de ambiente finalizada")

    def __setup_folder(self, local_folder):
        """
        Cria diretorio para armazenar os CSV de acordo com o cliente
        :type local_folder: str
        """
        if not os.path.exists(local_folder):
            os.makedirs(local_folder)

    def gdn_process(
            self,
            client_list,
            report_config
    ):
        """
        Inicia e coordena todo o processo que envolve aquisição de dados
        """
        self.__setup()

        logging.debug("[GDN] Inicio do processo.")

        for client_id in client_list:
            logging.debug("Iniciando autenticacao por client id: {0}".format(client_id))

            adwords_client = Authentication().get_adwords_client(client_customer_id=client_id)

            for reports in filter(lambda r: r.enabled, report_config):
                report_downloader = adwords_client.GetReportDownloader(version=reports.version)

                template_filename = reports.table_file
                local_folder = reports.local_folder

                logging.debug("Formatando o nome do arquivo local")
                file_name = "{0}_{1}.csv".format(
                    template_filename,
                    datetime.now().strftime("%Y%m%d%H%M%S%f")
                )
                logging.debug("Formatação concluida: {0}".format(file_name))

                logging.debug("Formatando caminho completo local")

                csv_path = "{0}\\{1}".format(
                    os.getcwd(),
                    local_folder
                )

                self.__setup_folder(csv_path)

                csv_path = "{0}\\{1}".format(
                    csv_path,
                    file_name
                )

                logging.debug("Formatação concluida: {0}".format(csv_path))

                logging.debug("Montagem de report iniciada.")

                print("Get GDN Report {0}".format(reports.report_type))
                logging.debug("Get GDN Report {0}".format(reports.report_type))

                print("Query Started: Client ID: {0} | Table file: {1}".format(client_id, reports.table_file))
                logging.debug("Query Started: Client ID: {0} | Table file: {1}".format(client_id, reports.table_file))

                report_query = {
                    'reportName': '{0} {1}'.format(reports.date_range, reports.report_type),
                    'dateRangeType': reports.date_range,
                    'reportType': '{0}'.format(reports.report_type),
                    'downloadFormat': 'CSV',
                    'selector': {
                        'fields': reports.fields
                        # 'dateRange': {  # for 'dateRangeType': "CUSTOM_DATE"
                        # 'min': '20181101',
                        # 'max': '20190107'
                        # }
                    }
                }

                logging.debug("Montagem de report concluida.")

                logging.debug("Recuperando conteúdo do arquivo CSV gerado pelo Report")
                response_media = report_downloader.DownloadReportAsString(report_query)
                logging.debug("Conteúdo recuperado, iniciando processo de normalização e parsing...")

                response_media = Sanitizer.normalize_response_media(
                    response_media=response_media,
                    need_sql_adjustment=True
                )

                logging.debug("Normalização efetuada")

                logging.debug("Abrindo para escrita o arquivo CSV local")

                if len(response_media) > 3:  # Retirando reports vazios
                    with open(csv_path, 'w', newline='', encoding='utf-8') as csv_file:
                        csv_writer = csv.writer(
                            csv_file,
                            delimiter=';',
                            dialect="excel"
                        )
                        i_cost = 0
                        i_allconv = 0
                        for rows in response_media[1:-1]:  # Ignoring the header and footer
                            rows = rows.strip()
                            if response_media[1].find('All conv.') > -1\
                                    and re.search('\d+,?\d+.?\d+\Z$', rows) is not None:  # Regra para conversoes
                                rows = re.sub('(\d+),?(\d+.?\d+)\Z$', r'\1\2', rows)  # Retira virgula caso valor>=1000
                            columns = rows.split(";")
                            if rows == response_media[1]:
                                for i_cost, values in enumerate(columns):  # Formatando campo 'Cost'
                                    if values == 'Cost':
                                        break
                                    else:
                                        i_cost = False
                                for i_allconv, values in enumerate(columns):
                                    if values == 'All conv.':
                                        break
                                    else:
                                        i_allconv = False
                            else:
                                if i_cost:
                                    columns[i_cost] = float(columns[i_cost]) / 1000000
                                if i_allconv:
                                    columns[i_allconv] = float(columns[i_allconv])
                            csv_writer.writerow(columns)
                        logging.debug("Processo de escrita finalizado")
                    csv_file.close()

                    logging.debug("Escrita do arquivo CSV local finalizada, arquivo fechado")

                    print("DONE *** Query Done: Client ID: {0} | Table file: {1}\n".format(client_id, reports.table_file))
                    logging.debug("DONE *** Query Done: Client ID: {0} | Table file: {1}\n".format(client_id, reports.table_file))
                else:
                    print("WARNING *** Empty report: Client ID: {0}\n".format(client_id))
                    logging.debug("WARNING *** Empty report: Client ID: {0}\n".format(client_id))

    def acquisition(self):
        """
        Executa o processo de criação de arquivo CSV a partir de uma fonte de dados
        """
        process = GdnProcess()
        for report in config.projects["GDN"]:
            client_list = report["client_custumer_id"]
            report_config = report["cfg"]
            try:
                process.gdn_process(
                    client_list,
                    report_config
                )
            except Exception as ex:
                logging.exception(ex)

    def mssql_store(self, project=config.projects["GDN"], bulk_insert=True, field_terminator=";"):
        super(GdnProcess, self).mssql_store(project=project)


if __name__ == '__main__':
    process = GdnProcess()
    for report in config.projects["GDN"]:
        client_list = report["client_custumer_id"]
        report_config = report["cfg"]
        try:
            process.gdn_process(
                client_list,
                report_config
            )
        except Exception as ex:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
            msg = ''.join('!! ' + line for line in lines)
            logging.exception(msg)
            print(msg)
