#!/usr/bin/env python
# -*- coding: utf-8 -*-
import config.gav4_config as config
import csv
import gav4
import logging
import logging.handlers
import os
import sys
import time
import traceback
import importlib

from authentication import Authentication
from datetime import datetime, date, timedelta
from abstract_process import AbstractProcess
from sanitizer import Sanitizer


class GaV4Process(AbstractProcess):
    def __init__(self):
        super(GaV4Process, self).__init__("GAV4")
        self.__setup_logger()

    def __setup_logger(self):
        """
        Inicializador de log para todo o processo de aquisição e upload de dados
        """
        log_path = "logs"
        if not os.path.exists(log_path):
            os.makedirs(log_path)

        log_handler = logging.handlers.WatchedFileHandler(
            "logs/{0}_log_{1}.log".format(
                "GAV4",
                date.today().strftime('%d%m%Y')
            )
        )
        formatter = logging.Formatter(
            '[%(levelname)s] %(asctime)s - %(name)s \n\t%(message)s',
            '%Y/%m/%d %H:%M:%S'
        )

        formatter.converter = time.gmtime
        log_handler.setFormatter(formatter)
        logger = logging.getLogger()
        logger.handlers = []
        logger.addHandler(log_handler)
        logger.setLevel(logging.DEBUG)

    def __setup(self):
        """
        Inicializa ambiente para executar o processo
        """
        self.__setup_logger()

        logging.debug("[GAV4] Reconfiguração de ambiente iniciada")
        stdin, stdout = sys.stdin, sys.stdout
        importlib.reload(sys)
        sys.stdin, sys.stdout = stdin, stdout
        # sys.setdefaultencoding("latin-1")
        logging.debug("[GAV4] Reconfiguração de ambiente finalizada")

    def __setup_folder(self, local_folder):
        """
        Cria diretorio para armazenar os CSV de acordo com o cliente
        :type local_folder: str
        """
        if not os.path.exists(local_folder):
            os.makedirs(local_folder)

    def __find_in_array(self, array, key, value):
        return [x for x in array if x[key] == value]

    def __get_account_summaries(self, service, max_results):
        """
        Retorna lista de contas do Google Analytics disponíveis para acesso
        :type service: Resource
        :type max_results: int
        """
        data_query = service.management().accountSummaries().list(**{
            u"max_results": int(max_results)
        })
        account_summaries = data_query.execute()
        return account_summaries

    def __get_gav4_data(self, service, request_body):
        """
        Realiza o request de dados ao Google Analytics
        :type service: Resource
        :type request_body: dict
        """
        ga_data = service.reports().batchGet(body=request_body).execute()
        return ga_data

    def __get_ga_header(self, column_headers):
        """
        Monta o cabeçalho do arquivo CSV com os dados do Google Analytics
        :type column_headers: list
        """
        ga_header = [[u"profileId"]]
        for column_header in column_headers:
            ga_header[0].append(column_header[u"name"].replace("ga:", ""))
        ga_header[0].append(u'reportName')
        ga_header[0].append(u'containsSampledData')
        ga_header[0].append(u'sampleSize')
        ga_header[0].append(u'sampleSpace')
        return ga_header

    def __get_ga_body(
            self,
            ga_data,
            profile_id,
            report_name
    ):
        """
        Monta o corpo do arquivo CSV com os dados do Google Analytics
        :type ga_data: dict
        """
        ga_body = []
        for index, data_row in enumerate(ga_data[u"rows"]):
            ga_body.append([profile_id])
            for data in data_row:
                ga_body[len(ga_body) - 1].append(Sanitizer.normalize_unicode_data(data, "sql", limit_char=500))
            ga_body[len(ga_body) - 1].append(report_name)
            ga_body[len(ga_body) - 1].append(
                ga_data.get("containsSampledData") if ga_data.get("containsSampledData") else "false"
            )
            ga_body[len(ga_body) - 1].append(
                ga_data.get("sampleSize") if ga_data.get("sampleSize") else 0
            )
            ga_body[len(ga_body) - 1].append(
                ga_data.get("sampleSpace") if ga_data.get("sampleSpace") else 0
            )
        return ga_body

    def __get_next_page_token(self, report_data):
        """
        Retonar o inicio do indice para a proxima pagina da extracao, caso exista
        :param report_data: dict
        :return: int
        """
        next_page_token = 0
        if "nextPageToken" in report_data:
            next_page_token = report_data["nextPageToken"]
        return int(next_page_token)

    def __get_gav4_report(
            self,
            service,
            profile_id,
            request_body,
            page_size,
            report_name="",
            days=7
    ):
        """
        Agrupa os reports diários vindos do Google Analytics, juntando um header com vários dados de n dias
        :type service: Resource
        :type days: int
        """
        report_builder = []
        # days = 1
        # date_limit = datetime.strptime('28112017', "%d%m%Y")  # datetime.today()
        for day in range(days):
            date_report = (datetime.today() - timedelta(days=day+1)).date()
            request_body["reportRequests"][0].update({
                "dateRanges": [
                    {
                        "startDate": str(date_report),
                        "endDate": str(date_report),
                    }
                ]
            })
            ga_data = self.__get_gav4_data(service, request_body)
            row_count = 0
            if "rowCount" in ga_data["reports"][0]["data"]:
                row_count = ga_data["reports"][0]["data"]["rowCount"]
            total_pages = int(row_count / page_size) + 1
            next_page_token = self.__get_next_page_token(ga_data["reports"][0])
            ga_data = gav4.convert_report(ga_data.get('reports', [])[0])  # V4 format to V3
            if row_count > 0:
                page = 1
                if day == 0:
                    report_builder = self.__get_ga_header(ga_data[u"columnHeaders"])
                while next_page_token > 0 or page <= total_pages:  # regras para paginação
                    print("\t\t\tReport: {0} {1} - Pagina: {2} - Final da página: {3} - Total registros: {4} \r".format(
                        report_name,
                        date_report,
                        page,
                        next_page_token if next_page_token > 0 else row_count,
                        row_count
                    ))
                    report_body = self.__get_ga_body(
                        ga_data,
                        profile_id,
                        report_name
                    )
                    report_builder += report_body[:]
                    if next_page_token > 0:
                        request_body["reportRequests"][0].update({
                            "pageToken": str(next_page_token)
                        })
                        ga_data = self.__get_gav4_data(service, request_body)
                        row_count = ga_data["reports"][0]["data"]["rowCount"]
                        total_pages = int(row_count / page_size) + 1
                        next_page_token = self.__get_next_page_token(ga_data["reports"][0])
                        ga_data = gav4.convert_report(ga_data.get('reports', [])[0])
                    else:
                        if "pageToken" in request_body["reportRequests"][0]:
                            del request_body["reportRequests"][0]["pageToken"]
                    page += 1
            # else:
            # raise ValueError("Os parâmetros específicados para o GA não trouxeram nenhum resultado...")
        return report_builder

    def ga_process(
            self,
            report_config
    ):
        """
        Inicia e coordena todo o processo que envolve aquisição de dados
        :type accounts_to_run: dict
        :type report_config: dict
        """
        self.__setup()

        service = Authentication().get_service_from_source("GAV4")

        logging.debug(
            "[GAV4] Inicio do processo"
        )

        for reports in filter(lambda r: r.enabled, report_config):
            template_filename = reports.table_file
            local_folder = reports.local_folder

            profile_id = reports.profile_id
            request_body = reports.request_body

            logging.debug("Iniciando Report: {0}".format(template_filename))

            logging.debug("Formatando o nome do arquivo local")

            file_name = "{0}_{1}.csv".format(
                template_filename,
                datetime.now().strftime("%Y%m%d%H%M%S%f")
            )

            logging.debug("Formatação concluida: {0}".format(file_name))

            logging.debug("Formatando caminho completo local")

            csv_path = "{0}\\{1}".format(
                os.getcwd(),
                local_folder
            )

            self.__setup_folder(csv_path)

            csv_path = "{0}\\{1}".format(
                csv_path,
                file_name
            )

            logging.debug("Formatação concluida: {0}".format(csv_path))

            logging.debug("Recuperando conteúdo do arquivo CSV gerado pelo Report")

            report_builder = self.__get_gav4_report(
                service,
                profile_id,
                request_body,
                reports.page_size,
                reports.report_name,
                reports.days_to_load
            )

            if len(report_builder):
                logging.debug("Conteúdo recuperado.")

                logging.debug("Abrindo para escrita o arquivo CSV local")

                with open(csv_path,  'w', newline='', encoding='utf-8') as csv_file:
                    csv_writer = csv.writer(
                        csv_file,
                        delimiter=";",
                        dialect="excel"
                    )
                    csv_writer.writerows(report_builder)

                    logging.debug("Processo de escrita finalizado")

                csv_file.close()

                logging.debug("Escrita do arquivo CSV local finalizada, arquivo fechado")
            else:
                logging.debug("Os parâmetros específicados para o GA não trouxeram nenhum resultado...")
            # Controlando os requests na geracao de varios relatorios
            if len(report_config) > 1:
                time.sleep(30)

    def acquisition(self):
        """
        Executa o processo de criação de arquivo CSV a partir de uma fonte de dados
        """
        process = GaV4Process()
        for report in config.projects["GAV4"]:
            report_config = report["cfg"]
            try:
                process.ga_process(
                    report_config
                )
            except Exception as ex:
                logging.exception(ex)

    def mssql_store(self, project=config.projects["GAV4"], bulk_insert=True, field_terminator=";"):
        super(GaV4Process, self).mssql_store(project=project)


if __name__ == "__main__":
    process = GaV4Process()
    for report in config.projects["GAV4"]:
        report_config = report["cfg"]
        try:
            process.ga_process(
                report_config
            )
        except Exception as ex:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
            msg = ''.join('!! ' + line for line in lines)
            logging.exception(msg)
            print (msg)
