#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.main_config import *


class GoogleSheetConfig:
    def __init__(
            self,
            spreadsheet_id,
            sheet_name,
            first_column_index,
            last_column_index,
            report_name="",
            enabled=False,
            sql_server_load_config=SqlServerLoadConfig()
    ):
        self.spreadsheet_id = spreadsheet_id
        self.sheet_name = sheet_name
        self.first_column_index = first_column_index
        self.last_column_index = last_column_index
        self.report_name = report_name
        self.enabled = enabled
        self.database = sql_server_load_config.database
        self.table_file = sql_server_load_config.table_file
        self.local_folder = sql_server_load_config.local_folder
        self.local_folder_loaded = sql_server_load_config.local_folder_loaded
        self.local_folder_error = sql_server_load_config.local_folder_error
        self.schema = sql_server_load_config.schema
        self.skip_rows = sql_server_load_config.skip_rows
        self.limit_days = sql_server_load_config.limit_days
        self.code_page = sql_server_load_config.code_page


# Google Spreadsheet IDs
FACEBOOK_ADS = '1p1_xelX5hd8Qg8ZPjcxTWW8mwFUkDAgc8V25ffKPy2E'

FACEBOOK_ADS_1 = '1a5gTGeqEONwSa7lqQo7C1d4vXvdD7KJFWZ_RDcIMCFg'
FACEBOOK_ADS_2 = '1cOL3NH8lDG5DVsIlO11UitCBSpysmQTok3T6PzZj8u4'
FACEBOOK_ADS_3 = '12LnJzVIJoNyxMocHpPOMbLRHsZm_90ii9JV3fpcsibA'

GA_ALL_SITE_DATA = '1v0-FRXKhAUXw-bHvnaKS7_UbNabUearl9ZGU0bRFjjQ'
# HISTORICO # '1A5RANj1qrMzPeE67wmTEw1IttC0QkuU3vPwGCyaTYWo'

BASE_CANAIS_MIDIA = '1744wFe8L2QZXc5qmur4jRP2dycfTMIRq7Tj0yhfqQSY'

projects = {
    "GOOGLE_SHEET": [{
        "cfg": [
            ###############################################
            #  EXTRAÇÕES FACEBOOK  #
            ###############################################
            GoogleSheetConfig(  # Base_Facebook_Visão_Geral
                spreadsheet_id=FACEBOOK_ADS_1,
                enabled=True,
                sheet_name="Visao_Geral",
                first_column_index="A",
                last_column_index="AP",
                report_name="Base_Facebook_Visão_Geral",
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_facebook_visao_geral",
                    local_folder="csvlocation\\Sheet\\ToLoad",
                    local_folder_loaded="csvlocation\\Sheet\\Loaded",
                    local_folder_error="csvlocation\\Sheet\\Error",
                    schema=[
                        {"name": "Date", "type": "STRING"}
                    ],
                    skip_rows=1
                )
            ),
            GoogleSheetConfig(  # Base_Facebook_Região
                spreadsheet_id=FACEBOOK_ADS_2,
                enabled=True,
                sheet_name="Regiao",
                first_column_index="A",
                last_column_index="AK",
                report_name="Base_Facebook_Região",
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_facebook_regiao",
                    local_folder="csvlocation\\Sheet\\ToLoad",
                    local_folder_loaded="csvlocation\\Sheet\\Loaded",
                    local_folder_error="csvlocation\\Sheet\\Error",
                    schema=[
                        {"name": "Date", "type": "STRING"}
                    ],
                    skip_rows=1
                )
            ),
            GoogleSheetConfig(  # Base_Facebook_Gênero
                spreadsheet_id=FACEBOOK_ADS_3,
                enabled=True,
                sheet_name="Genero",
                first_column_index="A",
                last_column_index="AL",
                report_name="Base_Facebook_Gênero",
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_facebook_genero",
                    local_folder="csvlocation\\Sheet\\ToLoad",
                    local_folder_loaded="csvlocation\\Sheet\\Loaded",
                    local_folder_error="csvlocation\\Sheet\\Error",
                    schema=[
                        {"name": "Date", "type": "STRING"}
                    ],
                    skip_rows=1
                )
            ),
            ###############################################
            #  EXTRAÇÕES GOOGLE ANALYTICS  #
            ###############################################
            GoogleSheetConfig(  # GA - Conversoes_BASE_EVENTS
                spreadsheet_id=GA_ALL_SITE_DATA,
                sheet_name="Conversoes_BASE_EVENTS",
                first_column_index="A",
                last_column_index="AD",
                report_name="Conversoes_BASE_EVENTS",
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_ga_base_events",
                    local_folder="csvlocation\\Sheet\\ToLoad",
                    local_folder_loaded="csvlocation\\Sheet\\Loaded",
                    local_folder_error="csvlocation\\Sheet\\Error",
                    schema=[
                        {"name": "Date", "type": "STRING"}
                    ],
                    skip_rows=1
                )
            ),
            GoogleSheetConfig(  # GA - Conversoes_BASE_METAS_GOAL_COMPLETIONS
                spreadsheet_id=GA_ALL_SITE_DATA,
                sheet_name="Conversoes_BASE_METAS_GOAL_COMPLETIONS",
                first_column_index="A",
                last_column_index="AD",
                report_name="Conversoes_BASE_METAS_GOAL_COMPLETIONS",
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_ga_base_metas_goal_completions",
                    local_folder="csvlocation\\Sheet\\ToLoad",
                    local_folder_loaded="csvlocation\\Sheet\\Loaded",
                    local_folder_error="csvlocation\\Sheet\\Error",
                    schema=[
                        {"name": "Date", "type": "STRING"}
                    ],
                    skip_rows=1
                )
            ),
            GoogleSheetConfig(  # GA - Tecnologia_BASE_DISPOSITIVO
                spreadsheet_id=GA_ALL_SITE_DATA,
                sheet_name="Tecnologia_BASE_DISPOSITIVO",
                first_column_index="A",
                last_column_index="AD",
                report_name="Tecnologia_BASE_DISPOSITIVO",
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_ga_base_dispositivo",
                    local_folder="csvlocation\\Sheet\\ToLoad",
                    local_folder_loaded="csvlocation\\Sheet\\Loaded",
                    local_folder_error="csvlocation\\Sheet\\Error",
                    schema=[
                        {"name": "Date", "type": "STRING"}
                    ],
                    skip_rows=1
                )
            ),
            GoogleSheetConfig(  # GA - Tecnologia_BASE_NAVEGADOR
                spreadsheet_id=GA_ALL_SITE_DATA,
                sheet_name="Tecnologia_BASE_NAVEGADOR",
                first_column_index="A",
                last_column_index="AC",
                report_name="Tecnologia_BASE_NAVEGADOR",
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_ga_base_navegador",
                    local_folder="csvlocation\\Sheet\\ToLoad",
                    local_folder_loaded="csvlocation\\Sheet\\Loaded",
                    local_folder_error="csvlocation\\Sheet\\Error",
                    schema=[
                        {"name": "Date", "type": "STRING"}
                    ],
                    skip_rows=1
                )
            ),
            GoogleSheetConfig(  # GA - Visao_Geral_BASE_HORA_DIA_SEMANA
                spreadsheet_id=GA_ALL_SITE_DATA,
                sheet_name="Visao_Geral_BASE_HORA_DIA_SEMANA",
                first_column_index="A",
                last_column_index="AD",
                report_name="Visao_Geral_BASE_HORA_DIA_SEMANA",
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_ga_base_hora_dia_semana",
                    local_folder="csvlocation\\Sheet\\ToLoad",
                    local_folder_loaded="csvlocation\\Sheet\\Loaded",
                    local_folder_error="csvlocation\\Sheet\\Error",
                    schema=[
                        {"name": "Date", "type": "STRING"}
                    ],
                    skip_rows=1
                )
            ),
            GoogleSheetConfig(  # GA - Visao_Geral_BASE_REGIAO
                spreadsheet_id=GA_ALL_SITE_DATA,
                sheet_name="Visao_Geral_BASE_REGIAO",
                first_column_index="A",
                last_column_index="AD",
                report_name="Visao_Geral_BASE_REGIAO",
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_ga_base_regiao",
                    local_folder="csvlocation\\Sheet\\ToLoad",
                    local_folder_loaded="csvlocation\\Sheet\\Loaded",
                    local_folder_error="csvlocation\\Sheet\\Error",
                    schema=[
                        {"name": "Date", "type": "STRING"}
                    ],
                    skip_rows=1
                )
            ),
            GoogleSheetConfig(  # GA - Visao_Geral_BASE_USUARIO
                spreadsheet_id=GA_ALL_SITE_DATA,
                sheet_name="Visao_Geral_BASE_USUARIO",
                first_column_index="A",
                last_column_index="AD",
                report_name="Visao_Geral_BASE_USUARIO",
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_ga_base_usuario",
                    local_folder="csvlocation\\Sheet\\ToLoad",
                    local_folder_loaded="csvlocation\\Sheet\\Loaded",
                    local_folder_error="csvlocation\\Sheet\\Error",
                    schema=[
                        {"name": "Date", "type": "STRING"}
                    ],
                    skip_rows=1
                )
            ),
            GoogleSheetConfig(  # GA - Visao_Geral_BASE_VISITS_GERAL
                spreadsheet_id=GA_ALL_SITE_DATA,
                sheet_name="Visao_Geral_BASE_VISITS_GERAL",
                first_column_index="A",
                last_column_index="AD",
                report_name="Visao_Geral_BASE_VISITS_GERAL",
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_ga_base_visits_geral",
                    local_folder="csvlocation\\Sheet\\ToLoad",
                    local_folder_loaded="csvlocation\\Sheet\\Loaded",
                    local_folder_error="csvlocation\\Sheet\\Error",
                    schema=[
                        {"name": "Date", "type": "STRING"}
                    ],
                    skip_rows=1
                )
            ),
            ###############################################
            #  EXTRAÇÕES CLIMATEMPO #
            ###############################################
            GoogleSheetConfig(  # Canais_Midia - Climatempo
                spreadsheet_id=BASE_CANAIS_MIDIA,
                enabled=False,
                sheet_name="Consolidação Canais",
                first_column_index="A",
                last_column_index="M",
                report_name="Canais_Midia_Climatempo",
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_canais_midia_climatempo",
                    local_folder="csvlocation\\Sheet\\ToLoad",
                    local_folder_loaded="csvlocation\\Sheet\\Loaded",
                    local_folder_error="csvlocation\\Sheet\\Error",
                    schema=[
                        {"name": "Date", "type": "STRING"}
                    ],
                    skip_rows=1
                )
            )
        ]
    }]
}
