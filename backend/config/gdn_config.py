#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.main_config import *


class GdnConfig:
    def __init__(
            self,
            version,
            report_type,
            fields,
            date_range,
            enabled=True,
            sql_server_load_config=SqlServerLoadConfig()
    ):
        self.version = version
        self.report_type = report_type
        self.fields = fields
        self.date_range = date_range
        self.enabled = enabled
        self.database = sql_server_load_config.database
        self.table_file = sql_server_load_config.table_file
        self.local_folder = sql_server_load_config.local_folder
        self.local_folder_loaded = sql_server_load_config.local_folder_loaded
        self.local_folder_error = sql_server_load_config.local_folder_error
        self.schema = sql_server_load_config.schema
        self.skip_rows = sql_server_load_config.skip_rows
        self.limit_days = sql_server_load_config.limit_days
        self.code_page = sql_server_load_config.code_page


# GDN Report params
REPORT_VERSION = "v201809"  # update version https://developers.google.com/adwords/api/docs/reference/
DATE_RANGE = "LAST_7_DAYS"  # date_range useful values: LAST_7_DAYS, THIS_MONTH, LAST_MONTH, ALL_TIME, CUSTOM_DATE
AD_REPORT = "AD_PERFORMANCE_REPORT"
AD_GROUP_REPORT = "ADGROUP_PERFORMANCE_REPORT"
CAMPAIGN_REPORT = "CAMPAIGN_PERFORMANCE_REPORT"
KEYWORD_REPORT = "KEYWORDS_PERFORMANCE_REPORT"

CRITERIA_REPORT = "CRITERIA_PERFORMANCE_REPORT"
SEARCH_QUERY_REPORT = "SEARCH_QUERY_PERFORMANCE_REPORT"
GEO_REPORT = "GEO_PERFORMANCE_REPORT"
AGE_RANGE_REPORT = "AGE_RANGE_PERFORMANCE_REPORT"
GENDER_REPORT = "GENDER_PERFORMANCE_REPORT"
CALL_METRICS_CALL_DETAILS_REPORT = "CALL_METRICS_CALL_DETAILS_REPORT"

# Client Custumer ID -> Relatorios que serao executados para cada id

ZEISS_BR_RAPP = "908-383-7880"
ZEISS_VISION_BR = "255-878-8115"

SUPERGASBRAS_ADS = "525-473-0808"

ADWORDS_CLIENT_LIST = [
    ZEISS_BR_RAPP,
    SUPERGASBRAS_ADS
]

projects = {
    "GDN": [{
        "client_custumer_id": ADWORDS_CLIENT_LIST,
        "cfg": [
            GdnConfig(  # All Info - KEYWORD_REPORT
                version=REPORT_VERSION,
                report_type=KEYWORD_REPORT,
                fields=[
                    'Date',
                    'DayOfWeek',
                    'Device',
                    'AccountDescriptiveName',
                    'CampaignId',
                    'CampaignName',
                    'AdGroupId',
                    'AdGroupName',
                    'CampaignStatus',
                    'AccountCurrencyCode',
                    # 'ConversionTrackerId',
                    'KeywordMatchType',
                    'QualityScore',
                    'CreativeQualityScore',
                    'PostClickQualityScore',
                    'TopOfPageCpc',
                    'EstimatedAddClicksAtFirstPositionCpc',
                    # 'EstimatedAddCostAtFirstPositionCpc'
                    'Criteria',
                    'BiddingStrategyType',
                    'Clicks',
                    'Impressions',
                    'Cost',
                    'AverageCpc',
                    'AveragePosition',
                    'Interactions',
                    'Conversions',
                    'AllConversions',
                    'CurrentModelAttributedConversions',
                    'VideoViews',
                    'ViewThroughConversions',
                    'VideoViewRate',
                    'VideoQuartile25Rate',
                    'VideoQuartile50Rate',
                    'VideoQuartile75Rate',
                    'VideoQuartile100Rate',
                    'SearchImpressionShare',
                    'SearchRankLostImpressionShare',
                    'SearchBudgetLostTopImpressionShare',
                    'GmailForwards',
                    'GmailSaves',
                    'GmailSecondaryClicks'
                ],
                date_range=DATE_RANGE,
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_adwords_visao_geral",
                    local_folder="csvlocation\\Adwords\\ToLoad",
                    local_folder_loaded="csvlocation\\Adwords\\Loaded",
                    local_folder_error="csvlocation\\Adwords\\Error",
                    schema=[
                        {"name": "Date", "type": "string"}
                    ],
                    skip_rows=1
                )
            ),
            GdnConfig(  # SEARCH_QUERY_REPORT
                version=REPORT_VERSION,
                report_type=SEARCH_QUERY_REPORT,
                fields=[
                    'Date',
                    'DayOfWeek',
                    'Device',
                    'AccountDescriptiveName',
                    'CampaignId',
                    'CampaignName',
                    'AdGroupId',
                    'AdGroupName',
                    'CampaignStatus',
                    'AccountCurrencyCode',
                    # 'ConversionTrackerId',
                    'KeywordTextMatchingQuery',
                    'Query',
                    'QueryTargetingStatus',
                    'FinalUrl',
                    'TrackingUrlTemplate',
                    'Clicks',
                    'Impressions',
                    'Cost',
                    'AverageCpc',
                    'AveragePosition',
                    'Interactions',
                    'Conversions',
                    'AllConversions',
                    'VideoViews',
                    'ViewThroughConversions',
                    'VideoViewRate',
                    'VideoQuartile25Rate',
                    'VideoQuartile50Rate',
                    'VideoQuartile75Rate',
                    'VideoQuartile100Rate'
                ],
                date_range=DATE_RANGE,
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_adwords_search_query",
                    local_folder="csvlocation\\Adwords\\ToLoad",
                    local_folder_loaded="csvlocation\\Adwords\\Loaded",
                    local_folder_error="csvlocation\\Adwords\\Error",
                    schema=[
                        {"name": "Date", "type": "string"}
                    ],
                    skip_rows=1
                )
            ),
            GdnConfig(  # GEO_PERFORMANCE_REPORT
                version=REPORT_VERSION,
                report_type=GEO_REPORT,
                fields=[
                    'Date',
                    'DayOfWeek',
                    'Device',
                    'AccountDescriptiveName',
                    'CampaignId',
                    'CampaignName',
                    'AdGroupId',
                    'AdGroupName',
                    'CampaignStatus',
                    'AccountCurrencyCode',
                    # 'ConversionTrackerId',
                    'CityCriteriaId',
                    'CountryCriteriaId',
                    'CustomerDescriptiveName',
                    'IsTargetingLocation',
                    'MetroCriteriaId',
                    'MostSpecificCriteriaId',
                    'RegionCriteriaId',
                    'Clicks',
                    'Impressions',
                    'Cost',
                    'AverageCpc',
                    'AveragePosition',
                    'Interactions',
                    'Conversions',
                    'AllConversions',
                    'VideoViews',
                    'ViewThroughConversions',
                    'VideoViewRate'
                ],
                date_range=DATE_RANGE,
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_adwords_geo",
                    local_folder="csvlocation\\Adwords\\ToLoad",
                    local_folder_loaded="csvlocation\\Adwords\\Loaded",
                    local_folder_error="csvlocation\\Adwords\\Error",
                    schema=[
                        {"name": "Date", "type": "string"}
                    ],
                    skip_rows=1
                )
            ),
            GdnConfig(  # AGE_RANGE_PERFORMANCE_REPORT
                version=REPORT_VERSION,
                report_type=AGE_RANGE_REPORT,
                fields=[
                    'Date',
                    'DayOfWeek',
                    'Device',
                    'AccountDescriptiveName',
                    'CampaignId',
                    'CampaignName',
                    'AdGroupId',
                    'AdGroupName',
                    'CampaignStatus',
                    'AccountCurrencyCode',
                    # 'ConversionTrackerId',
                    'Id',
                    'CustomerDescriptiveName',
                    'ExternalCustomerId',
                    'Status',
                    'Criteria',
                    'BiddingStrategyType',
                    'Clicks',
                    'Impressions',
                    'Cost',
                    'AverageCpc',
                    'Interactions',
                    'Conversions',
                    'AllConversions',
                    'VideoViews',
                    'ViewThroughConversions',
                    'VideoViewRate',
                    'VideoQuartile25Rate',
                    'VideoQuartile50Rate',
                    'VideoQuartile75Rate',
                    'VideoQuartile100Rate',
                    'GmailForwards',
                    'GmailSaves',
                    'GmailSecondaryClicks'
                ],
                date_range=DATE_RANGE,
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_adwords_age_range",
                    local_folder="csvlocation\\Adwords\\ToLoad",
                    local_folder_loaded="csvlocation\\Adwords\\Loaded",
                    local_folder_error="csvlocation\\Adwords\\Error",
                    schema=[
                        {"name": "Date", "type": "string"}
                    ],
                    skip_rows=1
                )
            ),
            GdnConfig(  # GENDER_PERFORMANCE_REPORT
                version=REPORT_VERSION,
                report_type=GENDER_REPORT,
                fields=[
                    'Date',
                    'DayOfWeek',
                    'Device',
                    'AccountDescriptiveName',
                    'CampaignId',
                    'CampaignName',
                    'AdGroupId',
                    'AdGroupName',
                    'CampaignStatus',
                    'AccountCurrencyCode',
                    # 'ConversionTrackerId',
                    'Id',
                    'CustomerDescriptiveName',
                    'ExternalCustomerId',
                    'Status',
                    'Criteria',
                    'BiddingStrategyType',
                    'Clicks',
                    'Impressions',
                    'Cost',
                    'AverageCpc',
                    'Interactions',
                    'Conversions',
                    'AllConversions',
                    'VideoViews',
                    'ViewThroughConversions',
                    'VideoViewRate',
                    'VideoQuartile25Rate',
                    'VideoQuartile50Rate',
                    'VideoQuartile75Rate',
                    'VideoQuartile100Rate',
                    'GmailForwards',
                    'GmailSaves',
                    'GmailSecondaryClicks'
                ],
                date_range=DATE_RANGE,
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_adwords_gender",
                    local_folder="csvlocation\\Adwords\\ToLoad",
                    local_folder_loaded="csvlocation\\Adwords\\Loaded",
                    local_folder_error="csvlocation\\Adwords\\Error",
                    schema=[
                        {"name": "Date", "type": "string"}
                    ],
                    skip_rows=1
                )
            ),
            GdnConfig(  # CALL_METRICS_CALL_DETAILS_REPORT
                version=REPORT_VERSION,
                report_type=CALL_METRICS_CALL_DETAILS_REPORT,
                fields=[
                    'Date',
                    'DayOfWeek',
                    'AccountDescriptiveName',
                    'CampaignId',
                    'CampaignName',
                    'AdGroupId',
                    'AdGroupName',
                    'CampaignStatus',
                    'AccountCurrencyCode',
                    'CallDuration',
                    'CallEndTime',
                    'CallerCountryCallingCode',
                    'CallerNationalDesignatedCode',
                    'CallStartTime',
                    'CallStatus',
                    'CallTrackingDisplayLocation',
                    'CallType'
                ],
                date_range=DATE_RANGE,
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="tb_adwords_call_metrics",
                    local_folder="csvlocation\\Adwords\\ToLoad",
                    local_folder_loaded="csvlocation\\Adwords\\Loaded",
                    local_folder_error="csvlocation\\Adwords\\Error",
                    schema=[
                        {"name": "Date", "type": "string"}
                    ],
                    skip_rows=1
                )
            )
        ]
    }]
}
