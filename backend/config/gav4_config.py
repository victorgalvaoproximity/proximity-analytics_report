#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.main_config import *


class GaV4Config:
    def __init__(
            self,
            profile_id,
            request_body,
            page_size,
            report_name="",
            days_to_load=7,
            enabled=True,
            sql_server_load_config=SqlServerLoadConfig()
    ):
        self.profile_id = profile_id
        self.request_body = request_body
        self.page_size = page_size
        self.report_name = report_name
        self.days_to_load = days_to_load
        self.enabled = enabled
        self.database = sql_server_load_config.database
        self.table_file = sql_server_load_config.table_file
        self.local_folder = sql_server_load_config.local_folder
        self.local_folder_loaded = sql_server_load_config.local_folder_loaded
        self.local_folder_error = sql_server_load_config.local_folder_error
        self.schema = sql_server_load_config.schema
        self.skip_rows = sql_server_load_config.skip_rows
        self.limit_days = days_to_load
        self.code_page = sql_server_load_config.code_page


PAGE_SIZE = 10000  # Máximo na API V4: 10000

"""
    Listagem de todas as contas/views do GA que serão utilizadas nas extrações
    "ga_view_id" > ID da View do GA para extração
    "ga_acc_name" > Nome para incluir no 'report_name' (IE: conta > wp > view) 
    "ga_acc_enabled" > Chave para habilitar/desabilitar contas específicas
    "ga_folder_name" > Nome da pasta onde os arquivos serão gerados
    "ga_metrics_goals" > Lista de goals que serão agregados nas métricas
"""
####################
# ACCOUNTS
####################
GA_ACCOUNT_VIEW_LIST = [
    # Visa Brazil > [20080] Vai de Visa > [20080] Vai de Visa - PRD - All Traffic
    {
        "ga_acc_enabled": True,
        "ga_view_id": "108957945",
        "ga_acc_name": "Visa Brazil > [20080] Vai de Visa > [20080] Vai de Visa - PRD - All Traffic",
        "ga_folder_name": "Visa",
        "ga_metrics_goals": [
            {'expression': 'ga:goal4Completions'},
            {'expression': 'ga:goal1Completions'},
            {'expression': 'ga:goal2Completions'},
            {'expression': 'ga:goal5Completions'},
            {'expression': 'ga:goal16Completions'}
        ]
    }
]

####################
# PERFORMANCE REPORTS METRICS & DIMENSIONS
####################
# Metrics
# Default (todos os reports)
GA_PERFORMANCE_REPORT_DEFAULT_METRICS = [
    {'expression': 'ga:users'},
    {'expression': 'ga:sessions'},
    {'expression': 'ga:sessionDuration'},
    {'expression': 'ga:bounces'},
    {'expression': 'ga:newUsers'}
]
# Dimensions
GA_PERFORMANCE_REPORT_DIMENSIONS_LIST = [
    {  # 01 - Performance - Visão Geral
        "report_enabled": True,
        "report_type": "Performance - Visao Geral",
        "report_file_name": "tb_ga_performance_visao_geral",
        "dimensions_list": [
            {'name': 'ga:date'},
            {'name': 'ga:sourceMedium'},
            {'name': 'ga:campaign'},
            {'name': 'ga:keyword'},
            {'name': 'ga:adContent'}
        ]
    },
    {  # 02 - Performance - Usuários
        "report_enabled": True,
        "report_type": "Performance - Usuarios",
        "report_file_name": "tb_ga_performance_usuarios",
        "dimensions_list": [
            {'name': 'ga:date'},
            {'name': 'ga:sourceMedium'},
            {'name': 'ga:campaign'},
            {'name': 'ga:keyword'},
            {'name': 'ga:adContent'},
            {'name': 'ga:userGender'},
            {'name': 'ga:userAgeBracket'}
        ]
    },
    {  # 03 - Performance - Região
        "report_enabled": True,
        "report_type": "Performance - Regiao",
        "report_file_name": "tb_ga_performance_regiao",
        "dimensions_list": [
            {'name': 'ga:date'},
            {'name': 'ga:sourceMedium'},
            {'name': 'ga:campaign'},
            {'name': 'ga:keyword'},
            {'name': 'ga:adContent'},
            {'name': 'ga:region'},
            {'name': 'ga:city'}
        ]
    },
    {  # 04 - Performance - Hora/Dia Semana
        "report_enabled": True,
        "report_type": "Performance - Hora-Dia Semana",
        "report_file_name": "tb_ga_performance_hora_dia_semana",
        "dimensions_list": [
            {'name': 'ga:date'},
            {'name': 'ga:sourceMedium'},
            {'name': 'ga:campaign'},
            {'name': 'ga:keyword'},
            {'name': 'ga:adContent'},
            {'name': 'ga:hour'},
            {'name': 'ga:dayOfWeek'}
        ]
    },
    {  # 05 - Performance - Navegador
        "report_enabled": True,
        "report_type": "Performance - Navegadores",
        "report_file_name": "tb_ga_performance_navegadores",
        "dimensions_list": [
            {'name': 'ga:date'},
            {'name': 'ga:sourceMedium'},
            {'name': 'ga:campaign'},
            {'name': 'ga:keyword'},
            {'name': 'ga:adContent'},
            {'name': 'ga:browser'},
            {'name': 'ga:operatingSystem'}
        ]
    },
    {  # 06 - Performance - Dispositivo
        "report_enabled": True,
        "report_type": "Performance - Dispositivos",
        "report_file_name": "tb_ga_performance_dispositivos",
        "dimensions_list": [
            {'name': 'ga:date'},
            {'name': 'ga:sourceMedium'},
            {'name': 'ga:campaign'},
            {'name': 'ga:keyword'},
            {'name': 'ga:adContent'},
            {'name': 'ga:deviceCategory'},
            {'name': 'ga:mobileDeviceMarketingName'},
            {'name': 'ga:mobileDeviceBranding'}
        ]
    }
]

####################
# USABILITY REPORTS METRICS & DIMENSIONS
####################
# Metrics
GA_USABILITY_REPORT_DEFAULT_METRICS = [
    {'expression': 'ga:totalEvents'},
    {'expression': 'ga:uniqueEvents'},
    {'expression': 'ga:pageviews'},
    {'expression': 'ga:sessions'},
    {'expression': 'ga:users'},
    {'expression': 'ga:bounces'},
    {'expression': 'ga:sessionDuration'}
]
# Dimensions
GA_USABILITY_REPORT_DIMENSIONS_LIST = [
    {  # 01 - Usabilidade - Visão Geral
        "report_enabled": True,
        "report_type": "Usabilidade - Visao Geral",
        "report_file_name": "tb_ga_usabilidade_visao_geral",
        "dimensions_list": [
            {'name': 'ga:date'},
            {'name': 'ga:sourceMedium'},
            {'name': 'ga:pagePath'},
            {'name': 'ga:eventCategory'},
            {'name': 'ga:eventAction'},
            {'name': 'ga:eventLabel'}
        ]
    },
    {  # 02 - Usabilidade - Usuários
        "report_enabled": True,
        "report_type": "Usabilidade - Usuarios",
        "report_file_name": "tb_ga_usabilidade_usuarios",
        "dimensions_list": [
            {'name': 'ga:date'},
            {'name': 'ga:sourceMedium'},
            {'name': 'ga:pagePath'},
            {'name': 'ga:eventCategory'},
            {'name': 'ga:eventAction'},
            {'name': 'ga:eventLabel'},
            {'name': 'ga:userGender'},
            {'name': 'ga:userAgeBracket'}
        ]
    },
    {  # 03 - Usabilidade - Região
        "report_enabled": True,
        "report_type": "Usabilidade - Regiao",
        "report_file_name": "tb_ga_usabilidade_regiao",
        "dimensions_list": [
            {'name': 'ga:date'},
            {'name': 'ga:sourceMedium'},
            {'name': 'ga:pagePath'},
            {'name': 'ga:eventCategory'},
            {'name': 'ga:eventAction'},
            {'name': 'ga:eventLabel'},
            {'name': 'ga:region'},
            {'name': 'ga:city'}
        ]
    },
    {  # 04 - Usabilidade - Hora/Dia Semana
        "report_enabled": True,
        "report_type": "Usabilidade - Hora-Dia Semana",
        "report_file_name": "tb_ga_usabilidade_hora_dia_semana",
        "dimensions_list": [
            {'name': 'ga:date'},
            {'name': 'ga:sourceMedium'},
            {'name': 'ga:pagePath'},
            {'name': 'ga:eventCategory'},
            {'name': 'ga:eventAction'},
            {'name': 'ga:eventLabel'},
            {'name': 'ga:hour'},
            {'name': 'ga:dayOfWeek'}
        ]
    },
    {  # 05 - Usabilidade - Navegador
        "report_enabled": True,
        "report_type": "Usabilidade - Navegadores",
        "report_file_name": "tb_ga_usabilidade_navegadores",
        "dimensions_list": [
            {'name': 'ga:date'},
            {'name': 'ga:sourceMedium'},
            {'name': 'ga:pagePath'},
            {'name': 'ga:eventCategory'},
            {'name': 'ga:eventAction'},
            {'name': 'ga:eventLabel'},
            {'name': 'ga:browser'},
            {'name': 'ga:operatingSystem'}
        ]
    },
    {  # 06 - Usabilidade - Dispositivo
        "report_enabled": True,
        "report_type": "Usabilidade - Dispositivos",
        "report_file_name": "tb_ga_usabilidade_dispositivos",
        "dimensions_list": [
            {'name': 'ga:date'},
            {'name': 'ga:sourceMedium'},
            {'name': 'ga:pagePath'},
            {'name': 'ga:eventCategory'},
            {'name': 'ga:eventAction'},
            {'name': 'ga:eventLabel'},
            {'name': 'ga:deviceCategory'},
            {'name': 'ga:mobileDeviceMarketingName'},
            {'name': 'ga:mobileDeviceBranding'}
        ]
    }
]

####################
# MONTAGEM DOS CONFIGURADORES DOS REPORTS
####################

# Variavel de lista de reports
reports_cfg_list = []
# Filtro de contas habilitadas
for ga_accounts in filter(lambda ac: ac["ga_acc_enabled"], GA_ACCOUNT_VIEW_LIST):
    # Filtro de reports PERFORMANCE habilitados
    for reports_performance in filter(lambda rp: rp["report_enabled"], GA_PERFORMANCE_REPORT_DIMENSIONS_LIST):
        metrics_performance_list = GA_PERFORMANCE_REPORT_DEFAULT_METRICS
        if len(ga_accounts["ga_metrics_goals"]) > 0:  # Goals dinamicos por conta
            metrics_performance_list = GA_PERFORMANCE_REPORT_DEFAULT_METRICS + ga_accounts["ga_metrics_goals"]
        reports_cfg_list.append(
            GaV4Config(
                page_size=PAGE_SIZE,
                enabled=True,
                profile_id=ga_accounts["ga_view_id"],
                request_body={
                    "reportRequests": [
                        {
                            "viewId": ga_accounts["ga_view_id"],
                            "samplingLevel": "LARGE",
                            "dimensions": reports_performance["dimensions_list"],
                            "metrics": metrics_performance_list,
                            "pageSize": PAGE_SIZE
                        }
                    ],
                },
                report_name="{0} - {1}".format(ga_accounts["ga_acc_name"], reports_performance["report_type"]),
                sql_server_load_config=SqlServerLoadConfig(
                    table_file=reports_performance["report_file_name"],
                    local_folder="csvlocation\\GA\\{0}".format(ga_accounts["ga_folder_name"]),
                    local_folder_error="csvlocation\\GA\\{0}\\Error".format(ga_accounts["ga_folder_name"])
                )
            )
        )

    # Filtro de reports USABILITY habilitados
    for reports_usability in filter(lambda ru: ru["report_enabled"], GA_USABILITY_REPORT_DIMENSIONS_LIST):
        metrics_usability_list = GA_USABILITY_REPORT_DEFAULT_METRICS
        reports_cfg_list.append(
            GaV4Config(
                page_size=PAGE_SIZE,
                enabled=True,
                profile_id=ga_accounts["ga_view_id"],
                request_body={
                    "reportRequests": [
                        {
                            "viewId": ga_accounts["ga_view_id"],
                            "samplingLevel": "LARGE",
                            "dimensions": reports_usability["dimensions_list"],
                            "metrics": metrics_usability_list,
                            "pageSize": PAGE_SIZE
                        }
                    ],
                },
                report_name="{0} - {1}".format(ga_accounts["ga_acc_name"], reports_usability["report_type"]),
                sql_server_load_config=SqlServerLoadConfig(
                    table_file=reports_usability["report_file_name"],
                    local_folder="csvlocation\\GA\\{0}".format(ga_accounts["ga_folder_name"]),
                    local_folder_error="csvlocation\\GA\\{0}\\Error".format(ga_accounts["ga_folder_name"])
                )
            )
        )

    # Report extra -- visao geral personalizada -- default enabled=False
    reports_cfg_list.append(
        GaV4Config(
            page_size=PAGE_SIZE,
            enabled=False,
            profile_id=ga_accounts["ga_view_id"],
            request_body={
                "reportRequests": [
                    {
                        "viewId": ga_accounts["ga_view_id"],
                        "samplingLevel": "LARGE",
                        "dimensions": [
                            {'name': 'ga:date'},
                            {'name': 'ga:sourceMedium'},
                            {'name': 'ga:campaign'},
                            {'name': 'ga:adContent'}
                        ],
                        "metrics": [
                            {'expression': 'ga:users'},
                            {'expression': 'ga:newUsers'},
                            {'expression': 'ga:sessions'},
                            {'expression': 'ga:avgSessionDuration'},
                            {'expression': 'ga:goal4Completions'},
                            {'expression': 'ga:goal1Completions'},
                            {'expression': 'ga:goal2Completions'},
                            {'expression': 'ga:goal5Completions'},
                            {'expression': 'ga:goal16Completions'}
                        ],
                        "pageSize": PAGE_SIZE
                    }
                ],
            },
            report_name="{0} - {1}".format(ga_accounts["ga_acc_name"], "Visao Geral Personalizada"),
            sql_server_load_config=SqlServerLoadConfig(
                table_file="tb_ga_performance_visao_geral_extra",
                local_folder="csvlocation\\GA\\{0}".format(ga_accounts["ga_folder_name"]),
                local_folder_error="csvlocation\\GA\\{0}\\Error".format(ga_accounts["ga_folder_name"])
            )
        )
    )

projects = {
    "GAV4": [{
        "cfg": reports_cfg_list
    }]
}
