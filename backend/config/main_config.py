import socks

from httplib2 import ProxyInfo


class SqlServerLoadConfig:
    def __init__(
            self,
            database="",
            table_file="",
            local_folder="",
            local_folder_loaded="",
            local_folder_error="",
            schema=[],
            skip_rows=0,
            limit_days=7,
            code_page="65001"  # utf-8 encode
    ):
        self.database = database
        self.table_file = table_file
        self.local_folder = local_folder
        self.local_folder_loaded = local_folder_loaded
        self.local_folder_error = local_folder_error
        self.schema = schema
        self.skip_rows = skip_rows
        self.limit_days = limit_days
        self.code_page = code_page


# SQL Server Databases
MSSQL_BI_ANALYTICS_REPORT = "DATABASE_NAME"

LOAD_CONTROL = False

enable_web_proxy = False
disable_ssl_certificate_validation = True
proxy_settings = ProxyInfo(
    proxy_type=socks.PROXY_TYPE_HTTP,
    proxy_host="localhost",
    proxy_port=8888
)
